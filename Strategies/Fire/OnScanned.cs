﻿namespace PK.Robocode.Basic.Strategies.Fire
{
    using Core.Utils;
    using PK.Robocode.Core;
    using PK.Robocode.Core.Model;
    using PK.Robocode.Core.Strategies.Fire;

    public class OnScanned : AbstractFireStrategy
    {
        private bool canFire;

        protected OnScanned(AbstractCoreRobot robot)
            : base(robot)
        {
            robot.EnemyScanned += (sender, scan) => { this.canFire = true; };

            this.Activated += (sender, args) => { this.Robot.IsAdjustGunForRobotTurn = false; };
        }

        public override void Do()
        {
            if (!this.canFire)
            {
                return;
            }

            this.canFire = false;

            var degree = this.GetAimingAngle(null);

            this.Robot.SetTurnGunRight(degree);
            this.Robot.SetFire(Definitions.MaxFirePower);
        }

        public override double CalcAimingAngle(IEnemy enemy, int tickOffset = 0)
        {
            return MathUtils.NormalRelativeAngleDegrees(this.Robot.RadarHeading);
        }
    }
}
﻿namespace PK.Robocode.Basic.Strategies.Fire
{
    using PK.Robocode.Core;
    using PK.Robocode.Core.Model;
    using PK.Robocode.Core.Strategies.Fire;

    public class HeadFake : AbstractFireStrategy
    {
        public HeadFake(AbstractCoreRobot robot)
            : base(robot)
        {
            this.Activated += (sender, args) =>
            {
                this.Robot.IsAdjustGunForRobotTurn = true;
            };
        }

        public override double CalcAimingAngle(IEnemy enemy, int tickOffset = 0)
        {
            return 0.0;
        }
    }
}
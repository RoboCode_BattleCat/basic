﻿namespace PK.Robocode.Basic.Strategies.Fire
{
    using PK.Robocode.Core;
    using PK.Robocode.Core.Model;
    using PK.Robocode.Core.Strategies.Fire;

    public class Area : AbstractFireStrategy
    {
        public Area(AbstractCoreRobot robot)
            : base(robot)
        {
        }

        public override double CalcAimingAngle(IEnemy enemy, int tickOffset = 0)
        {
            return 0.0;
        }
    }
}
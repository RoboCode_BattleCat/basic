﻿namespace PK.Robocode.Basic.Strategies.Fire
{
    using System;
    using Core.Utils;
    using PK.Robocode.Core;
    using PK.Robocode.Core.Model;
    using PK.Robocode.Core.Strategies.Fire;

    public class NonIterativeLinear : AbstractFireStrategy
    {
        public NonIterativeLinear(AbstractCoreRobot robot)
            : base(robot)
        {
            this.Activated += (sender, args) => { this.Robot.IsAdjustGunForRobotTurn = true; };
        }

        //public override void Do()
        //{
        //    var enemy = this.Robot.MostPriorityEnemy;

        //    if (enemy == null)
        //    {
        //        return;
        //    }

        //    var theta = MathUtils.DegreeToRadian(this.GetAimingAngle(enemy));
        //    if (theta == 0.0)
        //    {
        //        return;
        //    }

        //    this.Robot.SetTurnGunRightRadians(MathUtils.NormalRelativeAngle(theta - this.Robot.GunHeadingRadians));

        //    var firePower = this.DetermineFirePower(enemy);
        //    this.Robot.SetFire(firePower, this.BearingTolerance);
        //}

        public override double CalcAimingAngle(IEnemy enemy, int tickOffset = 0)
        {
            var firePower = this.DetermineFirePower(enemy);
            var bulletVelocity = MathUtils.BulletVelocity(firePower);

            // These constants make calculating the quadratic coefficients below easier
            var A = (enemy.Position.X - this.Robot.X) / bulletVelocity;
            var B = enemy.Velocity / bulletVelocity * Math.Sin(enemy.Heading);
            var C = (enemy.Position.Y - this.Robot.Y) / bulletVelocity;
            var D = enemy.Velocity / bulletVelocity * Math.Cos(enemy.Heading);

            // Quadratic coefficients: a*(1/t)^2 + b*(1/t) + c = 0
            var a = A * A + C * C;
            var b = 2 * (A * B + C * D);
            var c = B * B + D * D - 1;
            var discrim = b * b - 4 * a * c;

            if (!(discrim >= 0))
            {
                return 0.0;
            }

            // Reciprocal of quadratic formula
            var t1 = 2 * a / (-b - Math.Sqrt(discrim));
            var t2 = 2 * a / (-b + Math.Sqrt(discrim));
            var t = Math.Min(t1, t2) >= 0 ? Math.Min(t1, t2) : Math.Max(t1, t2);

            // Assume enemy stops at walls
            var endX = MathUtils.Limit(
                enemy.Position.X + enemy.Velocity * t * Math.Sin(enemy.Heading),
                this.Robot.Width / 2,
                this.Robot.BattleFieldWidth - this.Robot.Width / 2);

            var endY = MathUtils.Limit(
                enemy.Position.Y + enemy.Velocity * t * Math.Cos(enemy.Heading),
                this.Robot.Height / 2,
                this.Robot.BattleFieldHeight - this.Robot.Height / 2);

            var bearingTo = MathUtils.RadianToDegree(Math.Atan2(endX - this.Robot.X, endY - this.Robot.Y));

            return MathUtils.NormalRelativeAngleDegrees(bearingTo - this.Robot.GunHeading);
        }
    }
}
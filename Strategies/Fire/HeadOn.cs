﻿namespace PK.Robocode.Basic.Strategies.Fire
{
    using Core.Utils;
    using PK.Robocode.Core;
    using PK.Robocode.Core.Model;
    using PK.Robocode.Core.Strategies.Fire;

    public class HeadOn : AbstractFireStrategy
    {
        public HeadOn(AbstractCoreRobot robot)
            : base(robot)
        {
            this.Activated += (sender, args) =>
            {
                this.Robot.IsAdjustGunForRobotTurn = true;
            };
        }
        public override double CalcAimingAngle(IEnemy enemy, int tickOffset = 0)
        {
            var bearingTo = MathUtils.Bearing(this.Robot.Position, enemy.Position);

            return MathUtils.NormalRelativeAngleDegrees(bearingTo - this.Robot.GunHeading);
        }
    }
}
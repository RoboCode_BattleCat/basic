﻿namespace PK.Robocode.Basic.Strategies.Fire
{
    using Core.Utils;
    using PK.Robocode.Core;
    using PK.Robocode.Core.Model;
    using PK.Robocode.Core.Strategies.Fire;

    public class SimpleLinear : AbstractFireStrategy
    {
        public SimpleLinear(AbstractCoreRobot robot)
            : base(robot)
        {
            this.Activated += (sender, args) => { this.Robot.IsAdjustGunForRobotTurn = true; };
        }

        //public double FirePower { get; set; } = Definitions.MaxFirePower;

        //public override void Do()
        //{
        //    var enemy = this.Robot.MostPriorityEnemy;

        //    if (enemy == null)
        //    {
        //        return;
        //    }

        //    var firePower = this.DetermineFirePower(enemy);
        //    var bearingTo = this.GetAimingAngle(enemy);
        //    var degree = MathUtils.NormalRelativeAngleDegrees(bearingTo - this.Robot.GunHeading);

        //    this.Robot.SetTurnGunRight(degree);
        //    this.Robot.SetFire(firePower, this.BearingTolerance);
        //}

        public override double CalcAimingAngle(IEnemy enemy, int tickOffset = 0)
        {
            var firePower = this.DetermineFirePower(enemy);
            var distanceToEnemy = MathUtils.DistanceBetween(this.Robot.Position, enemy.Position);
            var time = MathUtils.BulletTravelTime(firePower, distanceToEnemy);
            var position = MathUtils.GetFuturePosition(enemy, time);
            var bearingTo = MathUtils.Bearing(this.Robot.Position, position);

            return MathUtils.NormalRelativeAngleDegrees(bearingTo - this.Robot.GunHeading);
        }
    }
}
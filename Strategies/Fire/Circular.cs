﻿namespace PK.Robocode.Basic.Strategies.Fire
{
    using System;
    using Core.Utils;
    using PK.Robocode.Core;
    using PK.Robocode.Core.Model;
    using PK.Robocode.Core.Strategies.Fire;

    public class Circular : AbstractFireStrategy
    {
        private double oldEnemyHeadingRadians;

        public Circular(AbstractCoreRobot robot)
            : base(robot)
        {
            this.Activated += (sender, args) => { this.Robot.IsAdjustGunForRobotTurn = true; };
        }

        //public override void Do()
        //{
        //    var enemy = this.Robot.MostPriorityEnemy;

        //    if (enemy == null)
        //    {
        //        return;
        //    }

        //    var theta = MathUtils.DegreeToRadian(this.GetAimingAngle(enemy));
        //    this.Robot.SetTurnGunRightRadians(MathUtils.NormalRelativeAngle(theta - this.Robot.GunHeadingRadians));

        //    var firePower = this.DetermineFirePower(enemy);
        //    this.Robot.SetFire(firePower, this.BearingTolerance);
        //}

        public override double CalcAimingAngle(IEnemy enemy, int tickOffset = 0)
        {
            var firePower = this.DetermineFirePower(enemy);
            var enemyHeadingChange = enemy.HeadingRadians - this.oldEnemyHeadingRadians;

            this.oldEnemyHeadingRadians = enemy.HeadingRadians;

            var deltaTime = 0;
            var predictedX = (double)enemy.Position.X;
            var predictedY = (double)enemy.Position.Y;

            while (++deltaTime * (20.0 - 3.0 * firePower)
                   < MathUtils.DistanceBetween(this.Robot.X, this.Robot.Y, predictedX, predictedY))
            {
                predictedX += Math.Sin(enemy.HeadingRadians) * enemy.Velocity;
                predictedY += Math.Cos(enemy.HeadingRadians) * enemy.Velocity;
                this.oldEnemyHeadingRadians += enemyHeadingChange;

                if (predictedX < 18.0 || predictedY < 18.0 || predictedX > this.Robot.BattleFieldWidth - 18.0
                    || predictedY > this.Robot.BattleFieldHeight - 18.0)
                {
                    predictedX = MathUtils.Limit(18.0, predictedX, this.Robot.BattleFieldWidth - 18.0);
                    predictedY = MathUtils.Limit(18.0, predictedY, this.Robot.BattleFieldHeight - 18.0);
                    break;
                }
            }

            var bearingTo = MathUtils.RadianToDegree(MathUtils.NormalAbsoluteAngle(Math.Atan2(predictedX - this.Robot.X, predictedY - this.Robot.Y)));

            return MathUtils.NormalRelativeAngleDegrees(bearingTo - this.Robot.GunHeading);
        }
    }
}
﻿namespace PK.Robocode.Basic.Strategies.Gathering
{
    using PK.Robocode.Core;
    using PK.Robocode.Core.Strategies.Gathering;

    public class Circular : AbstractGatheringStrategy
    {
        public Circular(AbstractCoreRobot robot)
            : base(robot)
        {
            this.Activated += (sender, args) =>
                {
                    this.Robot.IsAdjustRadarForGunTurn = true;
                    this.Robot.IsAdjustRadarForRobotTurn = true;
                };
        }

        public double TurnRadarSpeed { get; set; } = Definitions.MaxRadarTurnPerTurn;

        public override void Do()
        {
            this.Robot.SetTurnRadarRight(TurnRadarSpeed);
        }
    }
}
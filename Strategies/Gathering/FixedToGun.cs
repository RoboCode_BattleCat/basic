﻿namespace PK.Robocode.Basic.Strategies.Gathering
{
    using Core.Utils;
    using PK.Robocode.Core;
    using PK.Robocode.Core.Strategies.Gathering;

    public class FixedToGun : AbstractGatheringStrategy
    {
        public FixedToGun(AbstractCoreRobot robot)
            : base(robot)
        {
            this.Activated += (sender, args) =>
                {
                    robot.IsAdjustRadarForGunTurn = false;
                    robot.IsAdjustRadarForRobotTurn = false;
                };
        }

        public override void Do()
        {
            if (MathUtils.IsNear(this.Robot.GunHeading, this.Robot.RadarHeading))
            {
                return;
            }

            this.Robot.SetTurnRadarRight(MathUtils.NormalRelativeAngleDegrees(this.Robot.GunHeading));
        }
    }
}
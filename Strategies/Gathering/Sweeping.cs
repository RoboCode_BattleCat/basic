﻿namespace PK.Robocode.Basic.Strategies.Gathering
{
    using System.Collections.Generic;
    using System.Linq;
    using Core.Utils;
    using PK.Robocode.Core;
    using PK.Robocode.Core.Extensions;
    using PK.Robocode.Core.Model;
    using PK.Robocode.Core.Strategies.Gathering;

    public class Sweeping : AbstractGatheringStrategy
    {
        private IEnemy currentEnemy;

        private double scanDirection = 1.0;

        public Sweeping(AbstractCoreRobot robot)
            : base(robot)
        {
            this.Activated += (sender, args) => { this.scanDirection = 0; };
        }

        public double OverSweeping { get; set; } = 15;

        public override void Do()
        {
            var enemies = this.Robot.EnemiesAlive;

            var almostRightEnemy = enemies.MaxBy(
                e =>
                    {
                        var degreee = this.NormalRelativeAngleTo(e);
                        return degreee > 0 ? degreee : 0;
                    });

            var almostLeftEnemy = enemies.MinBy(
                e =>
                    {
                        var degreee = this.NormalRelativeAngleTo(e);
                        return degreee < 0 ? degreee : 0;
                    });

            // error fix
            if (almostLeftEnemy == almostRightEnemy && !enemies.Any())
            {
                if (this.scanDirection > 0)
                {
                    almostLeftEnemy = enemies.First(e => e != almostRightEnemy);
                }
                else if (this.scanDirection < 0)
                {
                    almostRightEnemy = enemies.First(e => e != almostLeftEnemy);
                }
            }

            if (this.scanDirection == 0)
            {
                this.currentEnemy = new List<IEnemy> { almostLeftEnemy, almostRightEnemy }.MinBy(
                        e => this.NormalRelativeAngleTo(e));

                this.scanDirection = this.currentEnemy == almostRightEnemy ? 1 : -1;
            }

            var degree = this.NormalRelativeAngleTo(this.currentEnemy);

            if (this.scanDirection > 0 && degree < 0 || this.scanDirection < 0 && degree > 0)
            {
                this.currentEnemy = this.scanDirection > 0 ? almostLeftEnemy : almostRightEnemy;

                this.scanDirection *= -1;
            }

            degree = this.NormalRelativeAngleTo(this.currentEnemy) + (this.scanDirection > 0 ? this.OverSweeping : -1 * this.OverSweeping);

            this.Robot.SetTurnRadarRight(degree);
        }

        private double NormalRelativeAngleTo(IEnemy enemy)
        {
            return MathUtils.NormalRelativeAngleDegrees(MathUtils.Bearing(this.Robot.Position, enemy.Position) - this.Robot.RadarHeading);
        }
    }
}
﻿namespace PK.Robocode.Basic.Strategies.Gathering
{
    using Core.Utils;
    using PK.Robocode.Core;
    using PK.Robocode.Core.Strategies.Gathering;

    public class PriorityTracking : AbstractGatheringStrategy
    {
        private int scanDirection;

        public PriorityTracking(AbstractCoreRobot robot)
            : base(robot)
        {
#if DEBUG
            this.Robot.KeyTyped += (sender, args) =>
                {
                    if (args.KeyChar.ToString().ToLower().Equals("o"))
                    {
                        this.Oscillating = !this.Oscillating;
                    }
                };
#endif

            this.Activated += (sender, args) =>
                {
                    this.Robot.IsAdjustRadarForRobotTurn = true;
                    this.Robot.IsAdjustRadarForGunTurn = true;
                };
        }

        public bool Oscillating { get; set; } = false;

        public double OscalationDegree { get; set; } = Definitions.MaxRadarTurnPerTurn;

        public override void Do()
        {
            var enemy = this.Robot.MostPriorityEnemy;

            if (enemy == null)
            {
                return;
            }

            var bearingTo = MathUtils.Bearing(this.Robot.Position, enemy.Position);
            var degree = MathUtils.NormalRelativeAngleDegrees(bearingTo - this.Robot.RadarHeading);

            if (!this.Oscillating)
            {
                this.Robot.SetTurnRadarRight(degree);
                return;
            }

            if (this.scanDirection == 0)
            {
                this.scanDirection = degree > 0 ? 1 : -1;
            }

            if (degree > 0 && degree > this.OscalationDegree)
            {
                this.scanDirection = 1;
            }

            if (degree < 0 && degree < this.OscalationDegree * -1)
            {
                this.scanDirection = -1;
            }

            this.Robot.SetTurnRadarRight(this.OscalationDegree * this.scanDirection);
        }
    }
}
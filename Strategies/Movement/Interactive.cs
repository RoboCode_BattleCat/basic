﻿namespace PK.Robocode.Basic.Strategies.Movement
{
    using System;
    using System.Collections.Generic;
    using PK.Robocode.Core;
    using PK.Robocode.Core.Strategies.Movement;

    public class Interactive : AbstractMovementStrategy
    {
        private const int keyLeft = 37;

        private const int keyRight = 39;

        private const int keyForward = 38;

        private const int keyBackward = 40;

        private readonly IList<int> pressedKeys = new List<int>();

        private DateTime lastTimeKeyPressed = DateTime.Now;

        public Interactive(AbstractCoreRobot robot)
            : base(robot)
        {
            this.Activated += (sender, args) => { this.pressedKeys.Clear(); };

            this.Robot.KeyPressed += (sender, args) =>
                {
                    if (DateTime.Now - this.lastTimeKeyPressed < TimeSpan.FromMilliseconds(1))
                    {
                        return;
                    }

                    var key = args.KeyCode;

                    if (!this.pressedKeys.Contains(key))
                    {
                        this.pressedKeys.Add(key);
                    }

                    this.lastTimeKeyPressed = DateTime.Now;

                    switch (key)
                    {
                        case keyLeft:
                            if (this.pressedKeys.Contains(keyRight))
                            {
                                this.pressedKeys.Remove(keyRight);
                            }
                            break;

                        case keyRight:
                            if (this.pressedKeys.Contains(keyLeft))
                            {
                                this.pressedKeys.Remove(keyLeft);
                            }
                            break;

                        case keyForward:
                            if (this.pressedKeys.Contains(keyBackward))
                            {
                                this.pressedKeys.Remove(keyBackward);
                            }
                            break;

                        case keyBackward:
                            if (!this.pressedKeys.Contains(keyForward))
                            {
                                this.pressedKeys.Remove(keyForward);
                            }
                            break;
                    }
                };

            this.Robot.KeyReleased += (sender, args) =>
                {
                    var key = args.KeyCode;

                    switch (key)
                    {
                        case keyLeft:
                        case keyRight:
                        case keyForward:
                        case keyBackward:
                            if (this.pressedKeys.Contains(key))
                            {
                                this.pressedKeys.Remove(key);
                            }
                            break;
                    }
                };
        }

        public override void Do()
        {
            if (this.pressedKeys.Contains(keyLeft))
            {
                this.Robot.SetTurnLeft(double.PositiveInfinity);
            }

            else if (this.pressedKeys.Contains(keyRight))
            {
                this.Robot.SetTurnRight(double.PositiveInfinity);
            }
            else
            {
                this.Robot.SetTurnRight(0);
            }

            if (this.pressedKeys.Contains(keyForward))
            {
                this.Robot.SetAhead(double.PositiveInfinity);
            }

            else if (this.pressedKeys.Contains(keyBackward))
            {
                this.Robot.SetBack(double.PositiveInfinity);
            }
            else
            {
                this.Robot.SetAhead(0);
            }
        }
    }
}
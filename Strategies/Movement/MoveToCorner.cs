﻿using System.Drawing;
using PK.Robocode.Core;
using PK.Robocode.Core.Strategies.Movement;

namespace PK.Robocode.Basic.Strategies.Movement
{
    public class MoveToCorner : AbstractMovementStrategy
    {
        public MoveToCorner(AbstractCoreRobot robot) : base(robot)
        {
            this.Corner = new PointF(
                (float)this.Robot.Width / 2.0F,
                (float)this.Robot.Height / 2.0F);
        }

        public PointF Corner { get; private set; }

        public bool CornerReached => this.Robot.DistanceTo(this.Corner) == 0;

        public bool Ready => this.CornerReached && this.Robot.Heading == 0;

        public override void Do()
        {
            if (this.CornerReached)
            {
                this.Robot.TurnTo(0);
            }
            else
            {
                this.Robot.MoveTo(Corner);
            }
        }
    }
}

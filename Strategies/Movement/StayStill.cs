﻿namespace PK.Robocode.Basic.Strategies.Movement
{
    using PK.Robocode.Core;
    using PK.Robocode.Core.Strategies.Movement;

    public class StayStill : AbstractMovementStrategy
    {
        public StayStill(AbstractCoreRobot robot)
            : base(robot)
        {
        }
    }
}
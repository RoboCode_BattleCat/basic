﻿namespace PK.Robocode.Basic.Strategies.Movement
{
    using System;
    using System.Drawing;
    using Core.Utils;
    using PK.Robocode.Core;
    using PK.Robocode.Core.Strategies.Movement;

    public class Random : AbstractMovementStrategy
    {
        /* The direction we are moving in */

        private static int moveDirection = 1;

        /* The speed of the last bullet that hit us, used in determining how far to move before deciding to change direction again. */

        private static double lastBulletSpeed = 15.0;

        private readonly System.Random random = MathUtils.GetRandom();

        /* How long we should continue to move in the current direction */

        private long moveTime = 1;

        /* How many times we have decided to not change direction. */

        private int sameDirectionCounter;

        private double wallStick = 120;

        public Random(AbstractCoreRobot robot)
            : base(robot)
        {
            this.Robot.HitByBullet += (sender, args) => { lastBulletSpeed = args.Velocity; };
        }

        public override void Do()
        {
            var enemy = this.Robot.MostPriorityEnemy;

            if (enemy == null)
            {
                return;
            }

            if (this.Robot.Time % 32 == 0)
            {
                /* Change the wall stick distance, to make us even more unpredictable */
                this.wallStick = 120 + this.random.NextDouble() * 40;
            }

            var absBearing = enemy.BearingRadians + this.Robot.HeadingRadians;
            var distance = enemy.Distance + (this.random.NextDouble() - 0.5) * 5.0;

            if (--this.moveTime <= 0)
            {
                distance = Math.Max(distance, 100 + this.random.NextDouble() * 50) * 1.25;
                this.moveTime = 50 + (long)(distance / lastBulletSpeed);

                ++this.sameDirectionCounter;

                /* Determine if we should change direction */
                if (this.random.NextDouble() < 0.5 || this.sameDirectionCounter > 16)
                {
                    moveDirection = -moveDirection;
                    this.sameDirectionCounter = 0;
                }
            }

            /* Move perpendicular to our enemy, based on our movement direction */
            var goalDirection = absBearing - Math.PI / 2.0 * moveDirection;

            /* This is too clean for crazy! Add some randomness. */
            goalDirection += (this.random.NextDouble() - 0.5) * (this.random.NextDouble() * 2.0 + 1.0);

            /* Smooth around the walls, if we smooth too much, reverse direction! */
            var x = this.Robot.X;
            var y = this.Robot.Y;
            var smooth = 0.0;

            /* Calculate the smoothing we would end up doing if we actually smoothed walls. */
            var fieldRect = new RectangleF(
                18f,
                18f,
                (float)this.Robot.BattleFieldWidth - 36,
                (float)this.Robot.BattleFieldHeight - 36);

            while (
                !fieldRect.Contains(
                    (float)(x + Math.Sin(goalDirection) * this.wallStick),
                    (float)(y + Math.Cos(goalDirection) * this.wallStick)))
            {
                /* turn a little toward enemy and try again */
                goalDirection += moveDirection * 0.1;
                smooth += 0.1;
            }

            var turn = MathUtils.NormalRelativeAngle(goalDirection - this.Robot.GunHeadingRadians);

            /* Adjust so we drive backwards if the turn is less to go backwards */
            if (Math.Abs(turn) > Math.PI / 2)
            {
                turn = MathUtils.NormalRelativeAngle(turn + Math.PI);
                this.Robot.SetBack(100);
            }
            else
            {
                this.Robot.SetAhead(100);
            }

            this.Robot.SetTurnRightRadians(turn);
        }
    }
}
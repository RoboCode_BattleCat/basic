﻿namespace PK.Robocode.Basic.Strategies.Movement
{
    using PK.Robocode.Core;
    using PK.Robocode.Core.Strategies.Movement;

    public class WallCrawler : AbstractMovementStrategy
    {
        private bool curveStarted;

        private int moveDirection = 1;

        private long timestamp;

        public WallCrawler(AbstractCoreRobot robot)
            : base(robot)
        {
            this.Robot.HitRobot += (sender, e) => { this.SwitchDirection(); };

            this.Robot.HitWall += (sender, e) => { this.SwitchDirection(); };

            this.Robot.BulletCollisionDetected += (sender, e) => { this.SwitchDirection(); };
        }

        public double WallDistance { get; set; }

        public override void Init()
        {
            this.Robot.TurnLeft(this.Robot.Heading - 90);
            this.Robot.SetTurnGunLeft(90);
        }

        public override void Do()
        {
            if (this.Robot.Velocity == 0)
            {
                this.SwitchDirection();
            }

            this.Robot.SetAhead(double.PositiveInfinity * this.moveDirection);
            var wallDistance = 125 + this.WallDistance;

            if (this.Robot.DistanceFrontWall - wallDistance > 1 && this.Robot.TurnRemaining == 0.0
                && this.Robot.Heading % 90 == 0)
            {
                this.curveStarted = true;
            }
            else if (this.Robot.DistanceFrontWall - wallDistance <= 1 && this.Robot.TurnRemaining == 0
                     && (this.Robot.Heading % 90 != 0 || this.curveStarted))
            {
                this.curveStarted = false;

                if (this.moveDirection > 0)
                {
                    this.Robot.SetTurnLeft(90 * this.moveDirection);
                }
                else
                {
                    this.Robot.SetTurnRight(90 * this.moveDirection);
                }
            }
        }

        private void SwitchDirection()
        {
            if (this.Robot.Time - this.timestamp <= 32)
            {
                return;
            }

            this.moveDirection = this.Robot.Velocity > 0 ? -1 : 1;
            this.timestamp = this.Robot.Time;
        }
    }
}
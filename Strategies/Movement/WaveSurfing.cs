﻿namespace PK.Robocode.Basic.Strategies.Movement
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using Core.Utils;
    using global::Robocode;

    using PK.Robocode.Core;
    using PK.Robocode.Core.Extra;
    using PK.Robocode.Core.Model;
    using PK.Robocode.Core.Strategies.Movement;

    public class WaveSurfing : AbstractMovementStrategy
    {
        private const int BINS = 47;

        private readonly double[] surfStats = new double[BINS];

        private IDictionary<IBullet, SurfingBulletScanExtensions> enemyWaves =
            new Dictionary<IBullet, SurfingBulletScanExtensions>();

        private RingBuffer<double> surfAbsBearings = new RingBuffer<double>(16);

        private RingBuffer<int> surfDirections = new RingBuffer<int>(16);

        public WaveSurfing(AbstractCoreRobot robot)
            : base(robot)
        {
            this.Robot.HitByBullet += this.HitByBullet;
            this.Robot.RoundEnded += this.RoundEnded;
            this.Robot.EnemyScanned += this.EnemyScanned;
            this.Robot.BulletScanned += this.BulletScanned;
        }

        public bool HasWaveToSurf => this.GetClosestSurfableWave() != null;

        public override void Do()
        {
            var surfWave = this.GetClosestSurfableWave();

            if (surfWave == null)
            {
                return;
            }

            var dangerLeft = this.CheckDanger(surfWave, -1);
            var dangerRight = this.CheckDanger(surfWave, 1);

            var goAngle = MathUtils.AbsoluteBearing(surfWave.FiredPosition, this.Robot.Position);
            if (dangerLeft < dangerRight)
            {
                goAngle = this.WallSmoothing(goAngle - Math.PI / 2, -1);
            }
            else
            {
                goAngle = this.WallSmoothing(goAngle + Math.PI / 2, 1);
            }

            this.Robot.SetBackAsFront(goAngle);
        }

        private void RoundEnded(object sender, RoundEndedEvent e)
        {
            this.surfDirections = new RingBuffer<int>(16);
            this.surfAbsBearings = new RingBuffer<double>(16);
            this.enemyWaves = new Dictionary<IBullet, SurfingBulletScanExtensions>();
        }

        private void EnemyScanned(object sender, EnemyScan enemyScan)
        {
            var enemy = this.Robot.GetEnemy(enemyScan.Name);

            this.surfDirections.Insert(0, enemy.LateralVelocity >= 0 ? 1 : -1);
            this.surfAbsBearings.Insert(0, enemy.AbsoluteBearingRadians + Math.PI);
        }

        private void BulletScanned(object sender, IBullet bullet)
        {
            if (this.surfDirections.Count <= 2)
            {
                return;
            }

            this.enemyWaves.Add(
                bullet,
                new SurfingBulletScanExtensions
                {
                    Direction = this.surfDirections[2],
                    DirectAngleRadians = this.surfAbsBearings[2]
                });
        }

        private void HitByBullet(object sender, IBullet bullet)
        {
            var enemy = bullet.FiredBy;
            // If the active bullets collection is empty, we must have missed the
            // detection of this wave somehow.
            if (!this.Robot.ActiveEnemyBullets.Any())
            {
                return;
            }

            var hitBulletLocation = bullet.CurrentPosition;
            IBullet hitWave = null;

            // look through the EnemyWaves, and find one that could've hit us.
            foreach (var b in this.Robot.ActiveEnemyBullets)
            {
                if (Math.Abs(b.DistanceTravaled - this.Robot.DistanceTo(bullet.FiredPosition))
                    < this.Robot.Height / 2
                    && MathUtils.IsNear(Math.Abs(MathUtils.BulletVelocity(bullet.Power) - bullet.Velocity), 0.001))
                {
                    hitWave = bullet;
                    break;
                }
            }

            if (hitWave != null)
            {
                this.LogHit(hitWave, hitBulletLocation);
            }
        }

        private IBullet GetClosestSurfableWave()
        {
            IBullet surfWave = null;
            var closestDistance = double.MaxValue;

            foreach (var bullet in this.Robot.ActiveEnemyBullets)
            {
                var distance = this.Robot.DistanceTo(bullet.FiredPosition) - bullet.DistanceTravaled;

                if (distance > bullet.Velocity && distance < closestDistance)
                {
                    surfWave = bullet;
                    closestDistance = distance;
                }
            }

            return surfWave;
        }

        // Given the wave that the bullet was on, and the point where we
        // were hit, calculate the index into our stat array for that factor.
        private int GetFactorIndex(IBullet bullet, PointF targetLocation)
        {
            var enemyWave = this.enemyWaves[bullet];

            var offsetAngle = MathUtils.AbsoluteBearing(bullet.FiredPosition, targetLocation) - enemyWave.DirectAngleRadians;
            var factor = MathUtils.NormalRelativeAngle(offsetAngle) / MathUtils.MaxEscapeAngle(bullet.Velocity)
                         * enemyWave.Direction;

            return (int)MathUtils.Limit(0, factor * ((BINS - 1) / 2) + (BINS - 1) / 2, BINS - 1);
        }

        // Given the EnemyWave that the bullet was on, and the point where we
        // were hit, update our stat array to reflect the danger in that area.
        private void LogHit(IBullet bullet, PointF targetLocation)
        {
            var index = this.GetFactorIndex(bullet, targetLocation);

            for (var x = 0; x < BINS; x++)
            {
                // for the spot bin that we were hit on, add 1;
                // for the bins next to it, add 1 / 2;
                // the next one, add 1 / 5; and so on...
                this.surfStats[x] += 1.0 / (Math.Pow(index - x, 2) + 1);
            }
        }

        private PointF PredictPosition(IBullet surfWave, int direction)
        {
            var predictedPosition = new PointF(this.Robot.Position.X, this.Robot.Position.Y);
            var predictedVelocity = this.Robot.Velocity;
            var predictedHeading = this.Robot.HeadingRadians;
            var maxTurning = 0.0;
            var moveAngle = 0.0;
            var moveDir = 0;

            var counter = 0; // number of ticks in the future
            var intercepted = false;

            do
            {
                // the rest of these code comments are rozu's
                moveAngle = this.WallSmoothing(
                    predictedPosition,
                    MathUtils.AbsoluteBearing(surfWave.FiredPosition, predictedPosition) + direction * (Math.PI / 2),
                    direction) - predictedHeading;
                moveDir = 1;

                if (Math.Cos(moveAngle) < 0)
                {
                    moveAngle += Math.PI;
                    moveDir = -1;
                }

                moveAngle = MathUtils.NormalRelativeAngle(moveAngle);

                // maxTurning is built in like this, you can't turn more then this in one tick
                maxTurning = Math.PI / 720d * (40d - 3d * Math.Abs(predictedVelocity));
                predictedHeading =
                    MathUtils.NormalRelativeAngle(predictedHeading + MathUtils.Limit(-maxTurning, moveAngle, maxTurning));

                // this one is nice ;). if predictedVelocity and moveDir have
                // different signs you want to breack down
                // otherwise you want to accelerate (look at the factor "2")
                predictedVelocity += predictedVelocity * moveDir < 0 ? 2 * moveDir : moveDir;
                predictedVelocity = MathUtils.Limit(-8, predictedVelocity, 8);

                // calculate the new predicted position
                predictedPosition = MathUtils.Project(predictedPosition, predictedHeading, predictedVelocity);

                counter++;

                if (MathUtils.DistanceBetween(predictedPosition, surfWave.FiredPosition)
                    < surfWave.DistanceTravaled + counter * surfWave.Velocity + surfWave.Velocity)
                {
                    intercepted = true;
                }
            }
            while (!intercepted && counter < 500);

            return predictedPosition;
        }

        private double CheckDanger(IBullet surfWave, int direction)
        {
            var index = this.GetFactorIndex(surfWave, this.PredictPosition(surfWave, direction));

            return this.surfStats[index];
        }

        private class SurfingBulletScanExtensions
        {
            public int Direction { get; set; }

            public double DirectAngleRadians { get; set; }
        }
    }
}
﻿using System.Drawing;
using PK.Robocode.Core;
using PK.Robocode.Core.Strategies.Movement;

namespace PK.Robocode.Basic.Strategies.Movement
{
    public class MoveToCenter : AbstractMovementStrategy
    {
        public MoveToCenter(AbstractCoreRobot robot) : base(robot)
        {
            this.Center = new PointF(
                (float)this.Robot.BattleFieldWidth / 2.0F,
                (float)this.Robot.BattleFieldHeight / 2.0F);
        }

        public PointF Center { get; private set; }

        public bool CenterReached => this.Robot.DistanceTo(Center) == 0;

        public bool Ready => this.CenterReached && this.Robot.Heading == 0;

        public override void Do()
        {
            if (this.CenterReached)
            {
                this.Robot.TurnTo(0);
            }
            else
            {
                this.Robot.MoveTo(Center);
            }
        }
    }
}

﻿namespace PK.Robocode.Basic.Strategies.Movement
{
    using Core.Utils;
    using PK.Robocode.Core;
    using PK.Robocode.Core.Strategies.Movement;

    public class Circler : AbstractMovementStrategy
    {
        private int moveDirection = 1;

        private long timestamp;

        public Circler(AbstractCoreRobot robot)
            : base(robot)
        {
        }

        public override void Do()
        {
            var enemy = this.Robot.MostPriorityEnemy;

            if (enemy == null)
            {
                return;
            }

            if (this.Robot.Velocity == 0)
            {
                this.SwitchDirection();
            }

            var margin = this.Robot.Velocity * 2;
            if (this.Robot.DistanceFrontWall < margin
             || this.Robot.DistanceBackWall < margin
             || this.Robot.DistanceLeftWall < margin
             || this.Robot.DistanceRightWall < margin)
            {
                this.SwitchDirection();
            }

            // squaring off
            this.Robot.SetTurnRight(MathUtils.NormalRelativeAngleDegrees(enemy.Bearing + 90));

            this.Robot.SetAhead(1000 * this.moveDirection);
        }

        private void SwitchDirection()
        {
            if (this.Robot.Time - this.timestamp <= 32)
            {
                return;
            }

            this.moveDirection = this.Robot.Velocity > 0 ? -1 : 1;
            this.timestamp = this.Robot.Time;
        }
    }
}
﻿namespace PK.Robocode.Basic.Strategies.Movement
{
    using System;
    using System.Drawing;
    using Core.Utils;
    using global::Robocode;

    using PK.Robocode.Core;
    using PK.Robocode.Core.Strategies.Movement;

    /// <summary>
    ///     <see cref="http://robowiki.net/wiki/Anti-Gravity_Tutorial"></see>
    /// </summary>
    public class AntiGravity : AbstractMovementStrategy
    {
        private double destinationAngle;
        private double xForce;
        private double yForce;

        public AntiGravity(AbstractCoreRobot robot)
            : base(robot)
        {
            this.Robot.Draw += this.Robot_Draw;
        }

        private void Robot_Draw(object sender, IGraphics e)
        {
            var degree = Math.Atan2(this.xForce, this.yForce);
            var point = MathUtils.PointOnCircle(
                100,
                (float)MathUtils.RadianToDegree(this.destinationAngle),
                this.Robot.Position);
            e.DrawLine(new Pen(Color.Red), this.Robot.Position, point);
        }

        public override void Do()
        {
            this.xForce = 0;
            this.yForce = 0;
            var enemies = this.Robot.EnemiesAlive;

            foreach (var enemy in enemies)
            {
                var absBearing = MathUtils.NormalAbsoluteAngle(Math.Atan2(enemy.Position.X - this.Robot.X, enemy.Position.Y - this.Robot.Y));
                var distance = enemy.Distance;
                this.xForce -= Math.Sin(absBearing) / (distance * distance);
                this.yForce -= Math.Cos(absBearing) / (distance * distance);
            }

            var angle = Math.Atan2(this.xForce, this.yForce);

            if (this.xForce == 0 && this.yForce == 0)
            {
            }
            else if (Math.Abs(angle - this.Robot.HeadingRadians) < Math.PI / 2)
            {
                this.destinationAngle =
                    this.WallSmoothing(MathUtils.NormalRelativeAngle(angle - this.Robot.HeadingRadians), 1);
                this.Robot.SetTurnRightRadians(this.destinationAngle);
                this.Robot.SetAhead(double.PositiveInfinity);
            }
            else
            {
                this.destinationAngle =
                    this.WallSmoothing(
                        MathUtils.NormalRelativeAngle(angle + Math.PI - this.Robot.HeadingRadians),
                        -1);
                this.Robot.SetTurnRightRadians(this.destinationAngle);
                this.Robot.SetAhead(double.NegativeInfinity);
            }
        }
    }
}